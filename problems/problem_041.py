# Complete the add_csv_lines function which accepts a list
# as its only parameter. Each item in the list is a
# comma-separated string of numbers. The function should
# return a new list with each entry being the corresponding
# sum of the numbers in the comma-separated string.
#
# These kinds of strings are called CSV strings, or comma-
# sepearted values strings.
#
# Examples:
#   * input:  []
#     output: []
#   * input:  ["3", "1,9"]
#     output: [3, 10]
#   * input:  ["8,1,7", "10,10,10", "1,2,3"]
#     output:  [16, 30, 6]
#
# Look up the string split function to find out how to
# split a string into pieces.

# Write out your own pseudocode to help guide you.

def add_csv_lines(csv_lines):
    #create new list variable
    new_list = []
    #loop through each item in list
    for entry in csv_lines:
        #set variable to sum numbers in string
        line_num = 0
        #set varibale to add comma split
        comma_split = entry.split(',')
        #loop through each entry 
        for comma in comma_split:
            #set varibale for value of numbers in comma seprated string
            value = int(comma)
            #set varibale to increment/count value of the numbers in comma seperated string
            line_num += value
        #append new list to incremented value results    
        new_list.append(line_num)
    #return new list
    return new_list
result = add_csv_lines(["8,1,7", "10,10,10", "1,2,3"])
print(result)

