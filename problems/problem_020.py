# Complete the has_quorum function to test to see if the number
# of the people in the attendees list is greater than or equal
# to 50% of the number of people in the members list.

def has_quorum(attendees_list, members_list):
    attendee = len(attendees_list)
    members = len(members_list)
    if attendee / members >= 0.5:
        return True
    else:
        return False
result = has_quorum(['attendees_list_check'],['members_list_check'])
print(result)
