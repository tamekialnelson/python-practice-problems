# Complete the calculate_sum function which accepts
# a list of numerical values and returns the sum of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#

def calculate_sum(values):
    sum = 0
    #if list of values is empty return none
    if len(values) == 0:
        return None
    for value in values:
        sum = sum + value
    return sum
results = calculate_sum([2,3,3,2,1])
print(results)