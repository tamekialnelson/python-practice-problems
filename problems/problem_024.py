# Complete the calculate_average function which accepts
# a list of numerical values and returns the average of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#
# Pseudocode is available for you

def calculate_average(values):
    #if len values == 0 in values list return None
    if len(values) == 0:
        return None
    #create varible for count
    count = 0
    #for each value in values list
    for value in values:
        #add value to count 
        count = count + value
    #return count divided by length of values    
    return count / len(values)
result = calculate_average([323,8,8])
print(result)
