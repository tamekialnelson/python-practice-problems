# Complete the minimum_value function so that returns the
# minimum of two values.
#
# If the values are the same, return either.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def minimum_value(value1, value2): #define function
    if value1 < value2:
        return(value1)
    else:
        return(value2)
min_value = minimum_value(48,9) #call function
print(min_value)    


# create function minimum_value (parameter values are val1 and val2) 
def minimum_value(val1,val2):
    
    #compare two values to determine minimum 
    if val1 < val2: 
        #if a value1 is less then do something -- either val1 is min or val2 is min
        return(val1) #return val1 if it is less else -- do something else
    else:
        return(val2)  #if a value2 is less, then return val2 
