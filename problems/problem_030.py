# Complete the find_second_largest function which accepts
# a list of numerical values and returns the second largest
# in the list
#
# If the list of values is empty, the function should
# return None
#
# If the list of values has only one value, the function
# should return None
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def find_second_largest(values):
    ## If the list of values is empty or has only one value, return None
    if len(values) <= 1:
        return None
    largest = values[0]
    second_largest = None
    for value in values[1:]:
        if value > largest:
            second_largest = largest
            largest = value
        elif value > second_largest:
            second_largest = value
         
    return second_largest
result = find_second_largest([2,5,3,7])
print(result)

