# Complete the find_indexes function which accepts two
# parameters, a list and a search term. It returns a new
# list that contains the indexes of the search term in
# the search list.
#
# Remember that indexes in Python are zero-based. That
# means the first element in the list is index 0.
#
# Examples:
#   * search_list:  [1, 2, 3, 4, 5]
#     search_term:  4
#     result:       [3]
#   * search_list:  [1, 2, 3, 4, 5]
#     search_term:  6
#     result:       []
#   * search_list:  [1, 2, 1, 2, 1]
#     search_term:  1
#     result:       [0, 2, 4]
#
# Look up the enumerate function to help you with this problem.

def find_indexes(search_list, search_term):
    # create a new empty list 
    search_result = []
    # loop through search list for index position (written as Int) and number associated with that index 
    for index_postion, num_in_list in enumerate(search_list):
        # if number in search list matches what is being searched for  
        if num_in_list == search_term:
            # append the index position to the empty list variable
            search_result.append(index_postion)
    # return new search result list
    return search_result
result = find_indexes([1, 2, 3, 4, 5], 4)
print(result)