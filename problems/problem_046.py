# Complete the make_sentences function that accepts three
# lists.
#   * subjects contains a list of subjects for three-word sentences
#   * verbs contains a list of verbs for three-word sentences
#   * objects contains a list of objects for three-word sentences
# The make_sentences function should return all possible sentences
# that can be made from the words in each list
#
# Examples:
#   * subjects: ["I"]
#     verbs:    ["play"]
#     objects:  ["Portal"]
#     returns:  ["I play Portal"]
#   * subjects: ["I", "You"]
#     verbs:    ["play"]
#     objects:  ["Portal", "Sable"]
#     returns:  ["I play Portal", "I play Sable",
#                "You play Portal", "You play Sable"]
#   * subjects: ["I", "You"]
#     verbs:    ["play", "watch"]
#     objects:  ["Portal", "Sable"]
#     returns:  ["I play Portal", "I play Sable",
#                "I watch Portal", "I watch Sable",
#                "You play Portal", "You play Sable"
#                "You watch Portal", "You watch Sable"]
#
# Do it without pseudocode, this time, from memory. Don't look
# at the last one you just wrote unless you really must.

def make_sentences(subjects, verbs, objects):
    # create new empty list to hold all of the sentences
    new_list = [] 
    #go through each subject to get each item
    for subject in subjects:
        # create varibale to store string 
        subject_string = subject
        #go through each verb to get each item
        for verb in verbs:
            # create varibale to store string 
            verb_string = verb 
             #go through each object to get each item
            for object in objects:
                # create varibale to store string 
                object_string = object      
                #combine subject, verb, object togther 
                new_list.append(f'{subject_string} {verb_string} {object_string}')
                
    #return all possible sentence outcome
    return new_list
result = make_sentences(["I", "You"], ["play", "watch"], ["Portal", "Sable"])
print(result)
